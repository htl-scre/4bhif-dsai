## Installationanleitung DSAI

1. [miniconda](https://docs.conda.io/en/latest/miniconda.html) installieren
2. Conda Prompt 
  * ```conda create -n dsai```
  * ```conda activate dsai```
  * ```conda install -c conda-forge jupyter scikit-learn seaborn```
